#pragma once

#include "board.hpp"
#include "game.hpp"
#include <iostream>

namespace io
{

int ask()
{
    std::cout << "Write a number of the cell you want." << std::endl;
    while (true) {
        int n;
        std::cin >> n;
        if (1 <= n && n <= 9) {
            return n;
        }
    }
}

void display(State s)
{
    std::cout << "Turn: " << s.turn + 1 << std::endl;
    s.board.print();
}
}  // namespace io
