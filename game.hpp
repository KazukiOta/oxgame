#pragma once

#include "board.hpp"

class State
{
public:
    State() : turn(0) {}

    void move();

    int turn;
    Board board;
};
