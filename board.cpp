#include "board.hpp"
#include <array>
#include <iostream>

bool Board::win(char player)
{
    for (int x = 0; x < 3; x++) {
        bool flagWin = true;
        for (int y = 0; y < 3; y++) {
            if (this->board.at(cordToSuf(x, y)) != player) {
                flagWin = false;
            }
        }
        if (flagWin) {
            return true;
        }
    }
    for (int y = 0; y < 3; y++) {
        bool flagWin = true;
        for (int x = 0; x < 3; x++) {
            if (this->board.at(cordToSuf(x, y)) != player) {
                flagWin = false;
            }
        }
        if (flagWin) {
            return true;
        }
    }

    bool flagWin1 = true;
    for (int i = 0; i < 3; i++) {
        if (this->board.at(cordToSuf(i, i)) != player) {
            flagWin1 = false;
        }
    }
    if (flagWin1) {
        return true;
    }

    bool flagWin2 = true;
    for (int i = 0; i < 3; i++) {
        if (this->board.at(cordToSuf(2 - i, 2 - i)) != player) {
            flagWin2 = false;
        }
    }
    if (flagWin2) {
        return true;
    }

    return false;
}

void Board::print()
{
    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 3; x++) {
            std::cout << this->board.at(cordToSuf(x, y));
        }
        std::cout << std::endl;
    }
}

void Board::set(int x, int y, char player)
{
    this->board.at(cordToSuf(x, y)) = player;
}
