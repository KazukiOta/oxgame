#include "board.hpp"
#include "game.hpp"
#include "io.hpp"

void State::move(char player)
{
    while (true) {
        int n = io.ask();
        int x = (x - 1) % 3;
        int y = (x - 1) / 3;
        char already = this->board.at(cordToSuf(x, y));
        if ('1' <= already && already <= '9') {
            this->board.set(x, y, player);
        }
    }
}
