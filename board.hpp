#pragma once

#include <array>

class Board
{
public:
    void reset()
    {
        this->board = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
    }

    bool win(char player);

    void set(int x, int y, char player);

    void print();

    int cordToSuf(int x, int y)
    {
        return 3 * y + x;
    }

private:
    std::array<char, 9> board;
};
